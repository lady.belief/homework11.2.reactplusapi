using System.Diagnostics;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region windows_services
// Говорим, что приложение можно хостить как вин-сервис
builder.Host.UseWindowsService()
    .ConfigureAppConfiguration((hostingContext, config) =>
    {
        var isService = !(Debugger.IsAttached || args.Contains("--console"));
        if (isService)
        {
            var processModule = Process.GetCurrentProcess().MainModule;
            if (processModule != null)
            {
                // Устанавливаем, что базовый адрес приложения 
                // текущая папка, а не C:\windows\system32
                config.SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? throw new InvalidOperationException());
                // Добавляем файл где будет размещены настройки приложения
                // например адрес хостинга
                config.AddJsonFile("hostsettings.json", optional: true);
            }
        }
    });
#endregion

const string origin = "MyAllowSpecificOrigins";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: origin,
        b =>
        {
            b.WithOrigins(builder.Configuration.GetSection("CORS:Origins").Get<string[]>())
                .SetIsOriginAllowed((host) => true)
                .WithHeaders(builder.Configuration.GetSection("CORS:Headers").Get<string[]>())
                .WithMethods(builder.Configuration.GetSection("CORS:Methods").Get<string[]>());
        });
});
builder.Services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/build"; });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseStaticFiles(); //позволяет обслуживать файлы

app.UseAuthorization();

app.UseRouting();
app.UseCors(origin);

//app.MapControllers();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.Run();
