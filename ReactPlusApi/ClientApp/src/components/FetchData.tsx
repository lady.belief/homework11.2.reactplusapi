import React from 'react';

type WeatherForecast = {
    date: any,
    temperatureC: number,
    temperatureF: number,
    summary: string
}
/*{forecasts.map(forecast => 
            <tr key={forecast.date}>
              <td>{forecast.date}</td>
              <td>{forecast.temperatureC}</td>
              <td>{forecast.temperatureF}</td>
              <td>{forecast.summary}</td>
            </tr>
          )}*/
function FetchData(forecasts: WeatherForecast[]) {
  return (
    <div>
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Date</th>
            <th>Temp. (C)</th>
            <th>Temp. (F)</th>
            <th>Summary</th>
          </tr>
        </thead>
        <tbody>
        <tr key={forecasts[0].date}>
              <td>{forecasts[0].date}</td>
              <td>{forecasts[0].temperatureC}</td>
              <td>{forecasts[0].temperatureF}</td>
              <td>{forecasts[0].summary}</td>
            </tr>
        </tbody>
      </table>
    </div>
  );
}

 export default FetchData

