import React, { useState } from 'react'
import FetchData from './FetchData'

type WeatherForecast = {
    date: any,
    temperatureC: number,
    temperatureF: number,
    summary: string
}

const defaultForecast: WeatherForecast[] = [{
    date: "00.00.00",
    temperatureC: 0,
    temperatureF: 0,
    summary: "none"
}]

const WeatherTable = () => {
    const [forecasts, setJsonForecasts] = useState<WeatherForecast[]>(() => [...defaultForecast]);
    const getForecasts = () => {
        try {
            fetch(`${process.env.REACT_APP_BACKEND_URL || ''}/WeatherForecast`).then(async response => {
                const j = await response.json();
                setJsonForecasts(forecasts => j);
            }).catch(e=>{
                console.log(JSON.stringify(e));
                setJsonForecasts(forecasts => defaultForecast);
            });
        } catch (e) {
            setJsonForecasts(forecasts => defaultForecast);
        }
    }

    const [json, setJson] = useState<{ text: string, error: boolean }>({ text: '', error: false });
    const get = () => {
        try {
            fetch(`${process.env.REACT_APP_BACKEND_URL || ''}/WeatherForecast`).then(async response => {
                const j = await response.json();
                console.log("Incoming data: ", j);
                setJson({ text: JSON.stringify(j), error: false });
            }).catch(e=>{
                console.log(JSON.stringify(e));
                setJson({ text: e.toString(), error: true });    
            });
        } catch (e) {
            setJson({ text: JSON.stringify(e), error: true });
        }
    }

    return (
        <div>
            <button onClick={get}>Test GET </button>
            <text>{json.text}</text>
            <br/><br/><br/>
            <button onClick={getForecasts}>Узнать погоду</button>
            <FetchData {...forecasts}/>
        </div>
    );
  }

  export default WeatherTable;