import React from 'react';
import WeatherTable from './components/Weather';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="Погода">
      <WeatherTable />
      </header>
    </div>
  );
}

export default App;
